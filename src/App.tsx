import React from 'react';
import ToDo from './modules/toDoApp';
import './App.css';

const App = () => {
  return (
    <ToDo/>
  );
}

export default App;
