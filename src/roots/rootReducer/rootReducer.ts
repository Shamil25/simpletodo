import { combineReducers } from 'redux';
import toDoReducer from "../../manegers/toDoReducer/reducer";

export type AppStateType = ReturnType<typeof toDoReducer>;

export default combineReducers({
    toDoReducer,
});