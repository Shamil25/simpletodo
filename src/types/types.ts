export type taskType = {
    id: number;
    text: string;
    isOpen: boolean;
}

export type InitialStateType = {
    tasks: Array<taskType>;
};
