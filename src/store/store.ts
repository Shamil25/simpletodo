import { createStore } from 'redux';
import toDoReducer from '../roots/rootReducer/rootReducer';

const store = createStore(
    toDoReducer,
);

export default store;