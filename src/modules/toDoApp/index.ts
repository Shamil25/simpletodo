import Component from './Todo';
import * as actions from './actions';
import * as selectors from './selectors';
import { connect } from 'react-redux';
import * as types from '../../types/types';


const mapStateToProps = (state: types.InitialStateType) => ({
    tasks: selectors.getAllTask(state)
});

const mapDispatchToProps = (dispatch: any) => ({
    saveTask: (task: types.taskType) => dispatch(actions.onAddTask(task)),
    deleteTask: (taskId: types.taskType) => dispatch(actions.onRemoveTask(taskId)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Component);
