import React,{ useState, FC } from 'react';
import TodoItem from './components/TodoItem';
import * as types from '../../types/types';

export interface Props {
    tasks: Array<types.InitialStateType>,
    saveTask: Function,
    deleteTask: Function,
}

const ToDo:FC<Props> = props => {
    const {
        tasks,
        saveTask,
        deleteTask,
    } = props

    const [inputText, setInputText] = useState('');

    const addTask = () => {

        const task: types.taskType = {
            id: new Date().getTime(),
            text: inputText,
            isOpen: true,
        }

        saveTask(task);
        setInputText('');
    }

     const onChangeText = (event: any) => {
        const newInputVal = event.target.value;
        setInputText(newInputVal)
     }

    return(
        <div className='container'>
            <div className="heading">
                <h1>To-Do List</h1>
            </div>
            <div className='form'>
                <input type="text"
                       value={inputText}
                       onChange={onChangeText}
                />
                <button onClick={addTask} className='addTaskBtn'>
                    <span>Add</span>
                </button>
            </div>
            <div>
                <ul>
                    { tasks.length ?
                        tasks.map((task: any) =>
                            task.isOpen &&
                            <TodoItem id={task.id}
                                  key={task.id}
                                  text ={task.text}
                                  deleteTask={deleteTask}
                            />): null
                    }
                </ul>
            </div>
        </div>
    );
}

export default React.memo(ToDo);
