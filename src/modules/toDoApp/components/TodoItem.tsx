import React,{ FC } from 'react';

export interface Props {
    id: string;
    text: string; 
    deleteTask: Function;
}

const TodoItem:FC<Props> = props => {
    const {
        id,
        text,
        deleteTask,
    } = props;

    const onDeleteTask = (event: any) => {
        deleteTask(event.target.id);
    }

    return(
        <div>
            <li id={id}
                  children={text}
             />
            <button
                id={id}
                onClick={onDeleteTask}
                children='Remove'
            />
        </div>
    );
}


export default React.memo(TodoItem);
