import { ActionTypes } from '../../actionTypes/actionTypes';
import * as types from '../../types/types';

export type AddTaskType = {
    type: ActionTypes.ADD_TASK;
    payload: types.taskType;
}

export type TaskDeleteType = {
    type: ActionTypes.TASK_DELETE;
    payload: types.taskType;
}

export const onAddTask = (task: types.taskType) => ({
    type: ActionTypes.ADD_TASK,
    payload: task
});

export const onRemoveTask = (taskId: types.taskType) => ({
    type: ActionTypes.TASK_DELETE,
    payload: taskId
});
