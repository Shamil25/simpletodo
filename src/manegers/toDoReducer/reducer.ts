import * as types from '../../types/types';
import { ActionTypes } from '../../actionTypes/actionTypes';


const initialState: types.InitialStateType = {
    tasks: [],
};

export default (state = initialState, action: any): types.InitialStateType => {
    switch (action.type) {
        case ActionTypes.ADD_TASK:
            return {
                ...state,
                tasks: [...state.tasks, action.payload],
            }
        case ActionTypes.TASK_DELETE:
            return {
                ...state,
                tasks: state.tasks.map(element => +action.payload === element.id ? { ...element, isOpen: false }: element)                
            }
        default:
            return state;        
    }
}