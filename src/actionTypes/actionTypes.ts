export enum ActionTypes {
    ADD_TASK = 'ADD_TASK',
    TASK_DELETE = 'TASK_DELETE',
}